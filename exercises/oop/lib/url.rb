# frozen_string_literal: true

# BEGIN
require 'uri'
require 'forwardable'

class Url
  attr_accessor :url
  attr_reader :query_params

  extend URI
  extend Forwardable
  include Comparable

  def_delegators :url, :scheme, :host

  def initialize(url)
    @url = URI.parse(url)
    @query_params = create_query_params
  end

  def <=>(other)
    url <=> other.url
  end

  def query_param(key, default_value = nil)
    @query_params[key] || default_value
  end

  private

  def create_query_params
    return nil if url.query.nil?

    query = URI.decode_www_form(url.query).to_h

    query.transform_keys(&:to_sym)
  end
end
# END
