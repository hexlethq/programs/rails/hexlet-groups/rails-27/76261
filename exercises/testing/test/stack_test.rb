# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new
  end

  def test_pop!
    @stack = Stack.new [1, 2]
    @stack.pop!

    assert_equal @stack.to_a, [1]
  end

  def test_pop_return
    @stack = Stack.new [1, 2]

    assert_equal @stack.pop!, 2
  end

  def test_pop_with_empty
    @stack = Stack.new

    assert_nil @stack.pop!
  end

  def test_push!
    @stack.push!(1)

    assert_equal @stack.to_a, [1]
  end

  def test_empty?
    assert_equal @stack.to_a, []
  end

  def test_to_a
    @stack = Stack.new [1]

    assert_equal @stack.to_a, [1]
  end

  def test_clear!
    @stack = Stack.new [1]

    assert_equal @stack.clear!, []
  end

  def test_size
    @stack = Stack.new [1]

    assert_equal @stack.size, 1
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
