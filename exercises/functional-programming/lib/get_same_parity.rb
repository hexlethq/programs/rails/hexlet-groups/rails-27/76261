# frozen_string_literal: true

# BEGIN
def get_same_parity(numbers)
  return [] if numbers.empty?

  filter_method = numbers[0].even? ? :even? : :odd?

  numbers.filter(&filter_method)
end
# END
