# frozen_string_literal: true

# BEGIN
def count_by_years(users)
  years = {}
  male_users = users.filter { |user| user[:gender] == 'male' }

  male_users.each do |user|
    year = user[:birthday][0, 4]

    if years.key? year
      years[year] += 1
    else
      years[year] = 1
    end
  end

  years
end
# END
