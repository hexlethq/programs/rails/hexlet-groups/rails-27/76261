# frozen_string_literal: true

# BEGIN
def anagramm_filter(test_word, list)
  splited_test_word = test_word.chars.sort

  list.filter do |word|
    splited_word = word.chars.sort

    splited_test_word == splited_word
  end
end
# END

p anagramm_filter('abba', %w[aabb abcd bbaa dada]) # ['aabb', 'bbaa']
p anagramm_filter('racer', %w[crazer carer racar caers racer]) # ['carer', 'racer']
p anagramm_filter('laser', %w[lazing lazy lacer]) # []
