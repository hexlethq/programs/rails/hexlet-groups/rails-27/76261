# frozen_string_literal: true

# BEGIN
def reverse(str)
  new_str = ''
  counter = str.size

  until counter.zero?
    new_str += str[counter - 1]
    counter -= 1
  end

  new_str
end
# END
