# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  return nil if start > stop

  result = ''
  counter = start

  while counter <= stop
    result += if (counter % 3).zero? && (counter % 5).zero?
                'FizzBuzz '
              elsif (counter % 3).zero?
                'Fizz '
              elsif (counter % 5).zero?
                'Buzz '
              else
                "#{counter} "
              end

    counter += 1
  end

  result.chop
end
# END
