# frozen_string_literal: true

# BEGIN
def fibonacci(number)
  return nil if number <= 0
  return 0 if number == 1

  index = 2
  fibonacci = [0, 1]

  while index < number
    fibonacci[index] = fibonacci[index - 1] + fibonacci[index - 2]
    index += 1
  end

  fibonacci.last
end
# END
