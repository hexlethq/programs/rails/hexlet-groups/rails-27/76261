# frozen_string_literal: true

# BEGIN
def build_query_string(params)
  sorted_keys = params.keys.sort

  sorted_keys.map { |key| "#{key}=#{params[key]}" }.join('&')
end
# END
