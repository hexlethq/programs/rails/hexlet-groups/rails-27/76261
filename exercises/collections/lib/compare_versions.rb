# frozen_string_literal: true

# BEGIN
def compare_versions(left, right)
  left_splitted = left.split('.').map(&:to_i)
  right_splitted = right.split('.').map(&:to_i)

  left_splitted <=> right_splitted
end
# END
